Technologies

We first listed down all the possible major technologies and frameworks and then did the evaluation for technologies for each member in the group to find 
out the best likely technologies to be used. 

Repository:
We used the following for our repository:
	- Git hub
	- Bitbucket 
	- Google code
	- ActiveCollab
	- Trac Ticketing
	The advantage of Bitbucket is that it has a good Backup. Should GitHub go down, the repository is still accessible. 
	If we experience some serious downtime we�ll just find a quick alternative web presence for our project. 
	

Server:
The following are used in the server side:
	- Java
	- MySQL
	- Hibernate
	- python
	Java is chosen as most members are familiar with it and the others (MySQL, Hibernate and Python) are
	are license free.

Client:
	- JavaScript
	- JQuery
	- CSS4
	Most members are also familiar with these technologies, hence were chosen.

Software Wiki:
	We used Bitbucket for this.


