-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2013 at 03:19 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tictactoe`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `description` text NOT NULL,
  `short_description` varchar(150) DEFAULT NULL,
  `time_till_expiry` int(11) NOT NULL,
  `last_join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gameboard`
--

CREATE TABLE IF NOT EXISTS `gameboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_fk` int(11) NOT NULL,
  `player_one_fk` int(11) NOT NULL,
  `player_two_fk` int(11) NOT NULL,
  `move_list` text NOT NULL,
  `last_move_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `playerlobby`
--

CREATE TABLE IF NOT EXISTS `playerlobby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_fk` int(11) NOT NULL,
  `lobby_fk` int(11) NOT NULL,
  `date_joined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) DEFAULT NULL,
  `nick_name` varchar(60) DEFAULT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `real_name` varchar(100) DEFAULT NULL,
  `last_log_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_activity_on` datetime DEFAULT NULL,
  `cookieKey` text COMMENT 'This field will track the activeCookie on the user',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nick_name` (`nick_name`,`email_address`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `avatar`, `nick_name`, `email_address`, `password`, `real_name`, `last_log_on`, `last_activity_on`, `cookieKey`) VALUES
(3, NULL, NULL, 'mikegrahamjones@gmail.com', 'd1cb279dc2df231374f411579d18c2a62e781fb925dbc014da3d888829141624', NULL, '2013-08-13 12:00:00', NULL, 'SdiE90xzJuqYstTKJlnfd2m56x7JsEW1p1PfqtcXH6g7yCACnX+WnqrYr5/d+HkhbISCHap2igvf\r\nKCJ0S92ZJcJn4BG9bFuI8oGuck7Vpf4G79xOx1YgolZldcF5Qk6wUQod+yL6TYC9SBAhS9X6TA=='),
(4, NULL, NULL, 'testuser@testemail.com', 'd1cb279dc2df231374f411579d18c2a62e781fb925dbc014da3d888829141624', NULL, '2013-08-15 10:04:27', NULL, 'Og2lX9P29TGr/NJNeh63PE+1SbBHQ2yWynSTirVo18WHcVW76Qfj1HZsIWdoz0AXfNGzUTtzw/oJ\r\n0t6DzL9HbUUZwNvzmUvnSfyKiu3y4Q+HkmuJtvPT7YyVC9YwHfHYWayDbOR8dIjDj1OZ3BKukA==');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
