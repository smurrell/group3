/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.ac.massey.scripts;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import nz.ac.massey.pattern.RegEx;

/**
 * Purpose of this class is simply to make life easy..
 * By automatically reading from the directories as supplied in the constructor
 * And including stylesheets and javascript.
 * @author Michael
 */
public class ScriptManager 
{
    //class variables
    private String urlPath;
    private StringBuilder allFiles = new StringBuilder();
    private HttpServletRequest request;
    //public constructor the ScriptManager
    public ScriptManager(HttpServletRequest request) throws IOException {
        this.request = request;
        this.ReadPaths("js");
        this.ReadPaths("styles");
    }
    //return the files
    public String getFiles() {
        return this.allFiles.toString();
    }
    //get direct stream to our directory in question
    private String getServletPath(String resourcePath) {
        ServletContext servletContext = request.getServletContext();
        return servletContext.getRealPath(resourcePath);
    }
    //setup the urlPath to the file in question
    private void setUrlPath(String Folder) {
        StringBuilder sb = new StringBuilder();
        //build a path via the hyperlink to the Folder
        sb.append("http://").append(request.getHeader("host")).append(request.getContextPath()).append('/').append(Folder).append('/');
        this.urlPath = sb.toString();
    }
    //get the file extension
    private String getFileExtension(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }
    //append a stylesheet to the stringbuilder
    private void appendStyleSheet(String filePath) {
        allFiles.append("<link rel='stylesheet' type='text/css' href='").append(filePath).append("' />\n");
    }
    //append some javascript to the stringbuilder
    private void appendJavaScript(String filePath) {
        allFiles.append("<script type='text/javascript' src='").append(filePath).append("'></script>\n");
    }
    //append the file to AllFiles
    private void appendFile(String File) {
        String ext = this.getFileExtension(File);
        StringBuilder urlPath = new StringBuilder();
        if(this.validateUri(File))
            urlPath.append(File);
        else urlPath.append(this.urlPath).append(File);
        if (ext.equals("css")) {
            this.appendStyleSheet(urlPath.toString());
        } else if (ext.equals("js")) {
            this.appendJavaScript(urlPath.toString());
        }
    }
    //read the directory of scripts or css files
    private void ReadPaths(String Folder) throws IOException {
        //load the paths from urls
        this.ReadCSV(Folder);
        this.setUrlPath(Folder);
        //start loading the local paths
        String sPath = this.getServletPath(Folder);
        File f = new File(sPath);
        File[] listOfFiles = f.listFiles();
        if (listOfFiles.length > 0) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    this.appendFile(listOfFiles[i].getName());
                }
            }
        }
    }
    //validate if we have a correct hyperlink
    private boolean validateUri( String resourceUri )
    {
        RegEx re = new RegEx(resourceUri, "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
        return re.Matches();
    }
    //read a CSV containing a list of hyperlinks
    private void ReadCSV(String CsvFile) throws IOException {
        //open the csv file
        StringBuilder csvPath = new StringBuilder();
        csvPath.append("/nz/ac/massey/scripts/resources/").append(CsvFile).append("_config.csv");
        System.out.println(csvPath.toString());
        InputStream csv = getClass().getResourceAsStream(csvPath.toString());
        //read from the file
        BufferedReader reader = new BufferedReader(new InputStreamReader(csv));
        //initialize line
        String line = null;
        //declare tokenizer
        StringTokenizer tokenizer;
        // read line by line
        while ((line = reader.readLine()) != null) {
            //initialize tokenizer
            tokenizer = new StringTokenizer(line, ",");
            // read token by token
            while (tokenizer.hasMoreTokens()) {
                String activeToken = tokenizer.nextToken();
                if (this.validateUri(activeToken)) {
                    this.appendFile(activeToken);
                }
            }
        }
        reader.close();
    }
}
