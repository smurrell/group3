/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.ac.massey.cs.capstone.group3;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.sql.Wrapper;

/**
 *
 * @author Sam
 */
@WebServlet(name = "Lobby", urlPatterns = {"/Lobby"})
public class Lobby extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */

            

            if (request.getParameter("getSId") != null) {
                Gson gson = new Gson();

                String json = gson.toJson("5ee97a48-b03d-4835-8aa9-735822f3751d");
                out.println(json);
            } else if (request.getMethod().equals("POST")) {
                Gson gson = new Gson();
                InputStream input = request.getInputStream();
                BufferedInputStream inStream = new BufferedInputStream(input);
                BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
                
                String rawIn = reader.readLine();
                String[] in = gson.fromJson(rawIn, String[].class);
                
                if(in[1].equals("newGame")){
                    //encode json object
                    Game testGame = new Game();
                    String json = gson.toJson(testGame);
                    out.println(json);
                }
                
            } else {
                
                

            }




            //decode json object
            /*
             Game raw = gson.fromJson(json, Game.class);
             out.println(raw);
             */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
