/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.ac.massey.cs.capstone.group3;

import java.util.UUID;

/**
 *
 * @author Sam
 */
public class Game {

    private String gameId;
    private String player1Id;
    private String player2Id;
    private int[][] gameArray;//0 unused, 1 player1, 2 player2

    public Game() {
        this.setup(null, null);
    }
    
    public Game(String player1ID, String player2ID)
    {
        this.setup(player1ID, player2ID);
    }
    
    public void setup(String player1ID, String player2ID)
    {
        this.player1Id = player1ID;
        this.player2Id = player2ID;
        
        gameId = UUID.randomUUID().toString();
        gameArray = new int[3][3];

        for (int row = 0; row < gameArray.length; row++) {
            for (int col = 0; col < gameArray[row].length; col++) {
                gameArray[row][col] = 0;

            }
        }
    }
    
    
    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getPlayer1Id() {
        return player1Id;
    }

    public void setPlayer1Id(String player1Id) {
        this.player1Id = player1Id;
    }

    public String getPlayer2Id() {
        return player2Id;
    }

    public void setPlayer2Id(String player2Id) {
        this.player2Id = player2Id;
    }

    public int getGameCount() {
        //TODO
        return 0;
    }

    public int[][] getGameArray() {
        return gameArray;
    }

    public void setGameArray(int x, int y, int move) {
        gameArray[x][y] = move;
    }

    @Override
    public String toString() {
        String list = "";
        for (int row = 0; row < gameArray.length; row++) {
            for (int col = 0; col < gameArray[row].length; col++) {
                 list += gameArray[row][col] + ",";

            }
        }
        
        return list;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.gameId != null ? this.gameId.hashCode() : 0);
        hash = 89 * hash + (this.player1Id != null ? this.player1Id.hashCode() : 0);
        hash = 89 * hash + (this.player2Id != null ? this.player2Id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Game other = (Game) obj;
        if ((this.gameId == null) ? (other.gameId != null) : !this.gameId.equals(other.gameId)) {
            return false;
        }
        if ((this.player1Id == null) ? (other.player1Id != null) : !this.player1Id.equals(other.player1Id)) {
            return false;
        }
        if ((this.player2Id == null) ? (other.player2Id != null) : !this.player2Id.equals(other.player2Id)) {
            return false;
        }
        return true;
    }
    
}
