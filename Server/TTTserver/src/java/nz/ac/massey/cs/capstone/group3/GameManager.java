/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.ac.massey.cs.capstone.group3;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Farhan
 */
public class GameManager{
    
    private static Map<String, Game> gameMap = new ConcurrentHashMap<String, Game>();
    
    public GameManager(){
        
    }
    
    public static void addGame(Game game)
    {
        gameMap.put(game.getGameId(), game);
    }
    
    public static Game getGame(String gameID)
    {
        return (Game) gameMap.get(gameID);
    }
    
    public static void deleteGame(String gameID)
    {
        gameMap.remove(gameID);
        
        // TODO:: Save the game to a database
    }
    
    
}
