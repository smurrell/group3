package nz.ac.massey.pattern;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Horspool text searching algorithm
 *
 * @author Michael
 */
public class Horspool {

    //declare variables
    private String p;
    private String word;
    private String[] meanings;
    private Map<Character, Integer> rightMostIndexes;

    /**
     * BooyerMore constructor
     *
     * @param p
     * @param t
     */
    public Horspool(String p, String t) {
        this.p = p;
        this.word = t; //store the word
        this.rightMostIndexes = updateMap(p);
    }
    /**
     * find the matches on the word and each word from meanings
     */
    public boolean Match()
    {
        return this.Matcher(p, word);
    } //close findMatches

    /**
     * Apply the horspool algorithm for string matching to return all words
     *
     * @param word ["p%t"]
     */
    private boolean Matcher(String p, String t)
    {
        int m = t.length();
        int n = p.length();
        //start the pointer off at 0
        int pointer = 0;
        //while of value of pointer + (totalString -1) is less than the length of the pattern
        while (pointer + (n - 1) < m) {
            //loop on each character from the pattern
            for (int indexInPattern = n - 1; indexInPattern >= 0; indexInPattern--) {
                //start an index on the text
                int indexInText = pointer + indexInPattern;
                //get the x pointer
                char x = t.charAt(indexInText);
                //get the y pointer
                char y = p.charAt(indexInPattern);
                //if indexInText is less than m
                if (indexInText >= m) {
                    break; //kill it
                }				//but if x is not equal to y
                if (x != y) {
                    //get the right
                    Integer r = rightMostIndexes.get(x);
                    //if the right is null
                    if (r == null) {
                        //update pointer
                        pointer = indexInText + 1;
                    } else {
                        //declare shift (right until right-most occurence)
                        int shift = indexInText - (pointer + r);
                        //update pointer
                        pointer += shift > 0 ? shift : 1;
                    }
                    break; //kill it
                } else if (indexInPattern == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Create a map based off the pattern in O(n)
     *
     * @param pattern
     * @return map
     */
    private Map<Character, Integer> updateMap(
            String pattern) {
        //declare new map
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        //loop over pattern
        for (int i = pattern.length() - 1; i >= 0; i--) {
            char c = pattern.charAt(i);
            //if c is at not in the map, then store it
            if (!map.containsKey(c)) {
                map.put(c, i);
            }
        } //for
        //return the finished map
        return map;
    } //close
}
