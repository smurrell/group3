package nz.ac.massey.pattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.*;

/**
 * Regular Expression validator
 *
 * @author Michael
 */
public class RegEx {

    private String text;
    private Pattern p;
    private String pattern; //create a pattern to only accept full local number format
    private Matcher m1;

    /**
     * Constructor to initialize variables
     *
     * @param i
     * @param pattern
     */
    public RegEx(String i, String pattern) {
        this.text = i;
        this.p = Pattern.compile(pattern); //compile the pattern
        this.m1 = p.matcher(text); //set up the first matcher
    }

    /**
     * return the results
     *
     * @return true/false
     */
    public boolean Matches() {
        return m1.matches();
    }
}
