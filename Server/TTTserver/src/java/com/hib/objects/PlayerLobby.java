/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hib.objects;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Michael
 */
public class PlayerLobby {

    private int id;
    private int playerFk;
    private int lobbyFk;
    private int status;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateJoined;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the playerFk
     */
    public int getPlayerFk() {
        return playerFk;
    }

    /**
     * @param playerFk the playerFk to set
     */
    public void setPlayerFk(int playerFk) {
        this.playerFk = playerFk;
    }

    /**
     * @return the lobbyFk
     */
    public int getLobbyFk() {
        return lobbyFk;
    }

    /**
     * @param lobbyFk the lobbyFk to set
     */
    public void setLobbyFk(int lobbyFk) {
        this.lobbyFk = lobbyFk;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the dateJoined
     */
    public Date getDateJoined() {
        return dateJoined;
    }

    /**
     * @param dateJoined the dateJoined to set
     */
    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }
}
