/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hib.objects;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 *
 * @author Michael
 */
public class PlayerBoard
{
    private int id;
    private int lobbyFk;
    private int playerFk;
    private int cellX;
    private int cellY;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPlayed;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the lobbyFk
     */
    public int getLobbyFk() {
        return lobbyFk;
    }

    /**
     * @param lobbyFk the lobbyFk to set
     */
    public void setLobbyFk(int lobbyFk) {
        this.lobbyFk = lobbyFk;
    }

    /**
     * @return the playerFk
     */
    public int getPlayerFk() {
        return playerFk;
    }

    /**
     * @param playerFk the playerFk to set
     */
    public void setPlayerFk(int playerFk) {
        this.playerFk = playerFk;
    }

    /**
     * @return the cellX
     */
    public int getCellX() {
        return cellX;
    }

    /**
     * @param cellX the cellX to set
     */
    public void setCellX(int cellX) {
        this.cellX = cellX;
    }

    /**
     * @return the cellY
     */
    public int getCellY() {
        return cellY;
    }

    /**
     * @param cellY the cellY to set
     */
    public void setCellY(int cellY) {
        this.cellY = cellY;
    }

    /**
     * @return the lastPlayed
     */
    public Date getLastPlayed() {
        return lastPlayed;
    }

    /**
     * @param lastPlayed the lastPlayed to set
     */
    public void setLastPlayed(Date lastPlayed) {
        this.lastPlayed = lastPlayed;
    }
}
