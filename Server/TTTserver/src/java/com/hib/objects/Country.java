/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hib.objects;

/**
 *
 * @author Michael
 */
public class Country
{
    private int id;
    private String location;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }
}
