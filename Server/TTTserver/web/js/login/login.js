function DisplayAdditionalFields()
{
	var Fields = $('#login ul li').each(function( index )
	{
		var mField = $(this);
		if(mField.hasClass('hidden'))
		{
			mField.hide();
			mField.removeClass('hidden');
			mField.addClass('warning');
			mField.slideDown('slow').show();
		}
	});
	UserName_su( false, 'danger', 'incorrect user name');
}

function UserName_su( ValidUserName, DangerStatus, ErrorMessage )
{
	var user_ListItem = $("#listUserName");
	FieldControl( user_ListItem, ValidUserName, DangerStatus );
}

function EmailAddress_su( ValidUserName, DangerStatus, ErrorMessage )
{
	var user_ListItem = $("#listUserName");
	FieldControl( user_ListItem, ValidUserName, DangerStatus );
}

function Password_su( ValidUserName, DangerStatus, ErrorMessage )
{
	var user_ListItem = $("#listUserName");
	FieldControl( user_ListItem, ValidUserName, DangerStatus );
}

function AddFieldMessage( MessageField, ErrorMessage )
{
	MessageField.hide();
	MessageField.removeClass('hidden');
	MessageField.addClass('warning');
	MessageField.slideDown('slow', function(){
	
		MessageField.html( ErrorMessage );
	
	}).show();
}

function FieldControl( field, validField, DangerStatus )
{
	if( field.hasClass("success") )
	{
		field.removeClass("success");
	}
	if( field.hasClass(DangerStatus) )
	{
		field.removeClass(DangerStatus);
	}
	if( validField )
	{
		field.addClass("success");
	}
	else
	{
		field.addClass(DangerStatus);
	}
}

$(document).ready(function()
{



});