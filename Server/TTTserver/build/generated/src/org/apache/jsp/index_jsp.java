package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import nz.ac.massey.scripts.ScriptManager;
import java.io.*;
import java.util.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!doctype html>\r\n");
      out.write("<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->\r\n");
      out.write("<!--[if lt IE 7]> <html class=\"no-js ie6 oldie\" lang=\"en\"> <![endif]-->\r\n");
      out.write("<!--[if IE 7]>    <html class=\"no-js ie7 oldie\" lang=\"en\"> <![endif]-->\r\n");
      out.write("<!--[if IE 8]>    <html class=\"no-js ie8 oldie\" lang=\"en\"> <![endif]-->\r\n");
      out.write("<!--[if IE 9]>    <html class=\"no-js ie9\" lang=\"en\"> <![endif]-->\r\n");
      out.write("<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->\r\n");
      out.write("<!--[if gt IE 9]><!--> <html class=\"no-js\" lang=\"en\" itemscope itemtype=\"http://schema.org/Product\"> <!--<![endif]-->\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>JSP Page</title>\r\n");
      out.write("        ");

            //automatic scripts and styles include management :)
            ScriptManager sm = new ScriptManager(request);
            out.print(sm.getFiles());
        
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("\t<div class=\"wrapper\">\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div id=\"header_wrapper\">\r\n");
      out.write("\t\t\t<div id=\"menu_wrapper\">\r\n");
      out.write("\t\t\t\t<h2 class='logo'>\r\n");
      out.write("\t\t\t\t\t<img src='styles/images/logo.png' />\r\n");
      out.write("\t\t\t\t</h2>\r\n");
      out.write("\t\t\t<ul class='navigation' >\r\n");
      out.write("\t\t\t</ul>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<ul id=\"promo_slider\">\r\n");
      out.write("\t\t\t\t<li><img src='styles/images/promo_slide.png' title='Play with friends!' /></li>\r\n");
      out.write("\t\t\t</ul>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("        <div id=\"primary_wrapper\"><div class=\"content_wrapper\">\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t\t\t<!-- Beign Page Layout Here -->\r\n");
      out.write("                <div class=\"row\">\r\n");
      out.write("\r\n");
      out.write("                    <article class=\"seven columns\">\r\n");
      out.write("\t\t\t\t\t\t<img src=\"styles/images/ttt.png\" />\r\n");
      out.write("\t\t\t\t   </article>\r\n");
      out.write("                    <article class=\"five columns right\">\r\n");
      out.write("                        <h3 class=\"left-center\">Getting Started...</h3>\r\n");
      out.write("                        <p class=\"left-center\">Welcome to the Tic Tac Toe Game created at Massey University, to get started simply login!</p>\r\n");
      out.write("\t\t\t\t\t\t<form id=\"login\" >\r\n");
      out.write("\t\t\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li class=\"field\" id=\"listUserName\" >\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<input class=\"text input\" type=\"text\" placeholder=\"Enter a Username\" id=\"inputUserName\" name=\"UserName\" />\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<p id=\"UserName_errormessage\" class=\"error hidden\" ></p>\r\n");
      out.write("\t\t\t\t\t\t\t\t</li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li class=\"field hidden\" id=\"listEmailAddress\" ><input class=\"email input\" type=\"email\" placeholder=\"Enter a Email Address\" id=\"inputEmailAddress\" name=\"EmailAddress\" /></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li class=\"field\" id=\"listPassword\" ><input class=\"password input\" type=\"password\" placeholder=\"Enter a Password\" id=\"inputPassword\" name=\"Password\" /></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li class=\"field hidden\" id=\"listPasswordConfirm\" >\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<input class=\"password input\" type=\"password\" placeholder=\"Confirm Password\" id=\"listPasswordConfirm\" name=\"PasswordConfirm\" />\r\n");
      out.write("\t\t\t\t\t\t\t\t</li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li class=\"field rememberme\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<label class=\"checkbox\" for=\"checkbox1\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<input name=\"checkbox1\" type=\"checkbox\" id=\"checkbox1\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<span></span> Remember me\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</label>\r\n");
      out.write("\t\t\t\t\t\t\t\t</li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li class=\"field\"><div class=\"medium metro rounded primary btn\"><input type=\"submit\" value=\"Submit\" /></div></li>\r\n");
      out.write("\t\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t\t</form>\r\n");
      out.write("                    </article>\r\n");
      out.write("                </div>\r\n");
      out.write("          \r\n");
      out.write("            <!-- Beign Page Layout Here -->\t\r\n");
      out.write("\t\t\t\r\n");
      out.write("        </div>\r\n");
      out.write("\t\t<div class=\"footer\">\r\n");
      out.write("\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t<article class=\"eight columns\">\r\n");
      out.write("\t\t\t\t\t\t<p class=\"left-center\">&#169; Massey University Group 3</p>\r\n");
      out.write("\t\t\t\t</article>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t\t\r\n");
      out.write("\r\n");
      out.write("\t\t\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
