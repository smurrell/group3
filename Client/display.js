var board = [];
var player = 0;
var turn = 0;


function createGame() {
	player = 1;
	document.getElementById('gamediv').innerHTML = '<h2>Player '+player+' won the coin toss!</h2>';
	var turn = 0;
	createTTTTable();
}

//Create Bot Function
function createBot() {

	x = Math.floor(Math.random()*3);
	y = Math.floor(Math.random()*3);

	coords = [x,y];
	if ( board[x][y] == 0 ) {
	
		var img = document.getElementById(coords);
		board[x][y] = player;
		img.setAttribute('src', 'Nought.jpg');
	} 
	else 
	{
		createBot();
	}
}

/* Create instance of TTT -scaleable, could be called 
for each cell in table */
function createTTTTable() {
	var body=document.getElementsByTagName('body')[0];
	var table=document.createElement('table')
	var tbdy=document.createElement('tbody');

	
	table.setAttribute('class', 'board');
	
	for(var i=0; i<3; i++){
		board[i] = [];
		var tr=document.createElement('tr');
		tr.setAttribute('class', 'cell');
		
		for(var j=0; j<3; j++){
			var coords = [i,j];
			board[i][j] = 0;
			var td=document.createElement('td');
			var img=document.createElement('img');
			img.setAttribute('class', 'cell');
			img.setAttribute('onclick','cellchoice('+coords+')');
			img.setAttribute('id', coords);
			img.setAttribute('src', 'Blank.jpg');
			td.appendChild(img); //"'"+board[i][j][0]+"'" once fixed width
			tr.appendChild(td);
		}
		tbdy.appendChild(tr);
	}
	table.appendChild(tbdy);
	body.appendChild(table);
}


function cellchoice(x,y){
//added additional clause to stop game if the game has been won.
	if (board[x][y] == 0 && checkboard()==false){
		var coords = [x,y]
		var img = document.getElementById(coords)
		board[x][y] = player

		if (player === 1){
			player = 2
			img.setAttribute('src', 'Cross.jpg');
			// Check wincase, adjust below accoridng
			checkboard();
			if (nottie()){
				document.getElementById('gamediv').innerHTML = "<h2>Player "+player+"'s turn</h2>";
				
				createBot();
				
				// Check wincase, adjust below accoridng
			checkboard();
			player = 1;
				if (nottie()){
					document.getElementById('gamediv').innerHTML = "<h2>Player "+player+"'s turn</h2>";
				}
				else {
				document.getElementById('gamediv').innerHTML = "<h2>The game is a tie!</h2>";
				}
			}
			else {
				document.getElementById('gamediv').innerHTML = "<h2>The game is a tie!</h2>";

			}
		}
		document.getElementById('gamediv').innerHTML = "<h2>Player "+winner+" won the game</h2>";
		
	}
}

// Function to check for Win and Tie
// horizontal win
function checkboard(){
	for(var x=0; x<3; x++)
	{
		if(board[x][0]==board[x][1]&&board[x][1]==board[x][2]&&board[x][0]!=0)
			{
			winner=board[x][0];
			return true;
			}
	}
	//vertical win
	for(var y=0; y<3; y++)
	{
		if(board[0][y]==board[1][y]&&board[1][y]==board[2][y]&&board[0][y]!=0)
			{
			winner=board[0][y];
			return true;
			}
	}
	
	//diagonal win
	if(board[0][0]==board[1][1]&&board[1][1]==board[2][2]&&board[0][0]!=0){winner=board[0][0];return true;}
	if(board[0][2]==board[1][1]&&board[1][1]==board[2][0]&&board[0][2]!=0){winner=board[0][2];return true;}
	
	// if no winner, check all cells if they are non 0 & wincase = 0. then game=tie
	
	return false;
}

// Check if no tie.
function nottie() {
	found = []
	for (var i = 0; i < 3; i++){
		found.push((board[i].indexOf(0) != -1));
	}
	return (found.indexOf(true) != -1)
}
